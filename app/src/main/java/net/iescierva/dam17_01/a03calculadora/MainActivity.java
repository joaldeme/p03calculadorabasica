package net.iescierva.dam17_01.a03calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    public TextView Display;
    public double num1, num2, result;
    boolean op2=false;
    int op;
    boolean punt=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Display=(TextView)findViewById(R.id.textView);

    }

    public void btn1(View v){

        String conca=Display.getText().toString();
        conca+="1";
        Display.setText(conca);

    }

    public void btn2(View v){

        String conca=Display.getText().toString();
        conca+="2";
        Display.setText(conca);

    }

    public void btn3(View v){

        String conca=Display.getText().toString();
        conca+="3";
        Display.setText(conca);

    }

    public void btn4(View v){

        String conca=Display.getText().toString();
        conca+="4";
        Display.setText(conca);

    }

    public void btn5(View v){

        String conca=Display.getText().toString();
        conca+="5";
        Display.setText(conca);

    }

    public void btn6(View v){

        String conca=Display.getText().toString();
        conca+="6";
        Display.setText(conca);

    }

    public void btn7(View v){

        String conca=Display.getText().toString();
        conca+="7";
        Display.setText(conca);

    }

    public void btn8(View v){

        String conca=Display.getText().toString();
        conca+="8";
        Display.setText(conca);

    }

    public void btn9(View v){

        String conca=Display.getText().toString();
        conca+="9";
        Display.setText(conca);

    }

    public void btn0(View v){

        String conca=Display.getText().toString();
        conca+="0";
        Display.setText(conca);

    }

    public void btnComa(View v) {

        if (punt==false) {
            String conca = Display.getText().toString();
            conca += ".";
            Display.setText(conca);
            punt = true;
        }
    }

    public void suma (View v){

        if (op2==false) {
            try {
                String aux1 = Display.getText().toString();
                num1 = Double.parseDouble(aux1);
                punt = false;
            } catch (NumberFormatException nfe) {
            }
        }
        else{
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                switch (op) {

                    case 1:
                        num1 = num1 + num2;
                        punt = false;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punt = false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punt = false;
                        break;
                    case 4:

                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;
                            punt = false;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay operandos suficientes", Toast.LENGTH_SHORT);
            }
        }
        Display.setText("");
        op=1;
        op2=true;
    }

    public void resta (View v){

        if (op2==false) {
            try {
                String aux1 = Display.getText().toString();
                num1 = Double.parseDouble(aux1);
                punt = false;
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                switch (op) {

                    case 1:
                        punt = false;
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punt = false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punt = false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;
                            punt = false;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay operandos suficientes", Toast.LENGTH_SHORT);
            }
        }
        Display.setText("");
        op=2;
        op2=true;

    }

    public void multi (View v){

        if (op2==false) {
            try {
                String aux1 = Display.getText().toString();
                num1 = Double.parseDouble(aux1);
                punt = false;
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                switch (op) {

                    case 1:
                        punt = false;
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punt = false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punt = false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;
                            punt = false;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay operandos suficientes", Toast.LENGTH_SHORT);
            }
        }
        Display.setText("");
        op=3;
        op2=true;

    }

    public void divi (View v){

        if (op2==false) {
            try {
                String aux1 = Display.getText().toString();
                num1 = Double.parseDouble(aux1);
                punt = false;
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = Display.getText().toString();
                num2 = Double.parseDouble(aux2);

                switch (op) {

                    case 1:
                        punt = false;
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punt = false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punt = false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            Display.setText("Error");
                        }
                        else{
                            result=num1/num2;
                            punt = false;
                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "No hay operandos suficientes", Toast.LENGTH_SHORT).show();
            }
        }
        Display.setText("");
        op=4;
        op2=true;

    }

    public void igual(View v){

        try{

            String aux2=Display.getText().toString();
            num2=Double.parseDouble(aux2);
            punt = true;
            op2=false;

        }catch(NumberFormatException nfe){
        }

        Display.setText("");

        if (op==1){
            result= num1+num2;
            punt = false;
        }
        else if (op==2){
            result= num1-num2;
            punt = false;
        }
        else if (op==3){
            result= num1*num2;
            punt = false;
        }
        else if (op==4){

            if (num2==Double.NaN) {
                Display.setText("Error");
            }
            else
                result=num1/num2;
                punt = false;
        }
        Display.setText(""+result);
        num1=result;
    }

    public void clear(View v){

        Display.setText("");
        num1=0.0;
        num2=0.0;
        result=0.0;

        op2=false;
        punt=false;

    }

    public void borrar(View v){

        if (!Display.getText().toString().equals("")){
            Display.setText("");
        }
        punt=false;

    }

}
